package com.example.apptrackerdemo.utils;

import android.net.TrafficStats;

/**
 * Created by Robert Zagórski on 2016-09-09.
 */
public class TrafficStatsHelper {


    public static long getAllRxBytes(int uid) { return TrafficStats.getUidRxBytes(uid);    }

    public static long getAllTxBytes(int uid) {
        return TrafficStats.getUidTxBytes(uid);
    }

    public static long getAllRxBytesMobile() {
        return TrafficStats.getMobileRxBytes();
    }

    public static long getAllTxBytesMobile() {
        return TrafficStats.getMobileTxBytes();
    }

    public static long getAllRxBytesWifi() {
        return TrafficStats.getTotalRxBytes() - TrafficStats.getMobileRxBytes();
    }

    public static long getAllTxBytesWifi() {
        return TrafficStats.getTotalTxBytes() - TrafficStats.getMobileTxBytes();
    }

    public static long getPackageRxBytes(int uid) {
        return TrafficStats.getUidRxPackets(uid);
    }

    public static long getPackageTxBytes(int uid) {
        return TrafficStats.getUidTxPackets(uid);
    }

}
